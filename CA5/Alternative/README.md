## CA5: Part 2: Alternative Solution

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/
>
> Create CA5 Alternative folder
>
>- mkdir CA5/Alternative
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA5/Alternative
>- echo “CA5: Alternative” >> README.md
>
> To make changes on the file use the command:
>
>- nano README.md
>
> Commit the changes and push
>
>- git add Alternative
>- git commit -m "created new directory CA5 Alternative (addresses #30)"
>- git push
>

**For this assignment you should present an alternative technological solution for the continuous integration tool (i.e., an alternative to Jenkins).**

> About Jenkins:
>
>- Jenkins is a continuous integration tool created to be a great build automation tool.
>- Self-managed free and open-source build automation and CI/CD developer tools
>- Continuous Integration (CI) and Continuous Delivery (CD)
>- Has thousands of plugins we can configure to build anything.
>- Jenkins is not the same as CloudBees. CloudBees is the company that primarily maintains Jenkins
>- Products used by the Jenkins community:
>
>  * Jenkins Open Source
>  * Jenkins X (run a pipeline out of the box with Kubernetes)
>  * CloudBees Jenkins Distribution (Jenkins environment built on the most recent supported Jenkins release with a catalog of tested plugins)
>  * CloudBees Core (paid tool)
>

> Bellow there’s a list with some alternative continuous integration tools. Some are open-source, others have a free plan for a certain number of users and other have paid only plans.
>
>- Some of these alternatives are "CI Only", which is the closest to Jenkins.
>- CI tools aren't natively equipped to be an end-to-end CI/CD tool because it was never designed as one.
>- CI/CD tools are designed to handle everything from source control to production.
>
>
> **OPEN-SOURCE:**
>
>- GoCD (Free & Open Source)
>
>
> **FREE PLAN TOOLS (up to 20 users):**
>
>- GitLab
>- GitHub (Microsoft)
>- Azure DevOps (Microsoft)
>- BuildMaster (Inedo)
>- TeamCity (JetBrains)
>- Circle CI
>- Buddy (Cloud-hosted)
>- Travis CI
>- Buildkite (Standard)
>
>
> **PAID PLANS ONLY TOOLS:**
>
>- Bamboo (Atlassian)
>- Buddy (Self-managed)
>- Drone
>- CloudBees CodeShip
>- AppVeyor
>- Semaphore
>- UrbanCode (IBM)
>
>
> For more information about Jenkins Alternatives for Continuous Integration see: https://blog.inedo.com/jenkins/alternatives-for-continuous-integration
>

You should Analyse how the alternative solution compares to your base solution.

> For this assignment the alternative chosen is **GitLab**
> 
>- GitLab allows a single application to handle everything the organization might need: for e.g.: service desk, issue tracking, wiki, source code repositories, code review, continuous integration, vulnerability management, feature flags, ChatOps, infrastructure as code, logging, error tracking, and security orchestration.
>- GitLab offers a list of technical criteria that Jenkins is missing
>

**1. Present how the alternative tool compares to Jenkins regarding continuous integration features;**

> Key differences:
>
> | Jenkins     | GitLab     |
> |-------------|------------|
> | Manage repositories, but up to some extent only, you might not have complete control over branches and other aspects | Manage Git repositories with complete control over branches and other aspects to keep your code secured and safe from threats |
> | “FREE OPEN SOURCE” and “Hosted-Internally” that’s why coders use it | “FREE” and “Self-Hosted, that’s why developers chose it |
> | Has change set supports and easy process for installation & configuration | Each project will have a tracker that will track the issue and perform code reviews to enhance efficiency |
> | Jenkins is for Continuous Integration | Gitlab is for Version Control and Code Collaboration |
>
>- More information on: https://www.educba.com/gitlab-ci-vs-jenkins/
>
> 
> Different features of Jenkins and GitLab:
>
> | Feature                        | Jenkins     | GitLab     |
> |--------------------------------|-------------|------------|
> | File name | Jenkinsfile (groovy) | .gitlab-ci.yml (yaml) |
> | Built-in CI/CD | Yes | Yes |
> | GitLab Self-monitoring | No | Yes |
> | Project Level Value Stream Analytics | No | Yes |
> | Group Level Value Stream Analytics | No | Yes |
> | Built-in Container Registry | No | Yes |
> | Preview your changes with Review Apps | No | Yes |
> | CI/CD Horizontal Autoscaling | No | Yes |
> | Operations Dashboard | Partially supported | Yes |
> | Built for using containers and Docker | No | Yes |
> | Cloud Native | No | Yes |
> | Container debugging with an integrated web terminal | No | Yes |
> | Comprehensive pipeline graphs | Yes | Yes |
> | Browsable artifacts | No | Yes |
> | Latest artifacts locked to prevent deletion | No | Yes |
> | Scheduled triggering of pipelines | Yes | Yes |
> | Code Quality MR Widget | Partially supported | Yes |
> | Code Quality Reports | Partially supported | Yes |
> | Code Quality violation notices in MR diffs | Partially supported | Yes |
> | Multi-project pipeline graphs | Partially supported | Yes |
> | Protected variables | No | Yes |
> | Environments and deployments | No | Yes |
> | Environments history | No | Yes |
> | Environment-specific variables | No | Yes |
> | Group-level variables | No | Yes |
> | Customizable path for CI/CD configuration | No | Yes |
> | Run CI/CD jobs on Windows | Yes | Yes |
> | Run CI/CD jobs on macOS | Yes | Yes |
> | Run CI/CD jobs on Linux ARM | Yes | Yes |
> | Run CI/CD jobs on FreeBSD | Yes | Yes |
> | Show code coverage rate for your pipelines | Yes | Yes |
> | Details on duration for each command execution in GitLab CI/CD | Yes | No |
> | Auto DevOps | No | Yes |
> | Protected Runners | No | Yes |
> | GitLab Agent for Kubernetes | No | Yes |
> | Fine-grained access controls for CI/CD based Kubernetes deployments | No | Yes |
> | GitOps deployment management | No | Yes |
> | Canary Deployments | No | Yes |
> | Automatic Retry for Failed CI Jobs | Yes | Yes |
> | Pipelines security | Yes | Yes |
> | Include external files in CI/CD pipeline definition | Yes | Yes |
> | Step folding for CI/CD logs | Yes | Yes |
> | Windows Container Executor | Yes | Yes |
> | Comments in Review Apps | No | Yes |
>
> For more detailed information about GitLab vs Jenkins we can see GitLab website: https://about.gitlab.com/devops-tools/jenkins-vs-gitlab/
>
> GitLab Docs:
> 
>- https://docs.gitlab.com/ee/
>

**2. Describe how the alternative tool could be used to solve the same goals as presented for this assignment (see previous slides);**

> MIGRATING FROM JENKINS:
>
>- https://docs.gitlab.com/ee/ci/migration/jenkins.html
>- https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html
>
> To use GitLab CI/CD, we need:
>
>- Application code hosted in a Git repository.
>- A file named `.gitlab-ci.yml` in the root of the repository, which contains the CI/CD configuration.
>
> In the .gitlab-ci.yml file, we can define:
>
>- The scripts we want to run.
>- Other configuration files and templates we want to include.
>- Dependencies and caches.
>- The commands we want to run in sequence and those you want to run in parallel.
>- The location to deploy the application to.
>- Whether we want to run the scripts automatically or trigger any of them manually.
>
> Bellow we have a prototype file for GitLabs equivalent to the Jenkinsfile from CA5/Part-2

```java 
image: java:11-jdk

stages:
  - checkout
  - assemble
  - test
  - generateJar
  - javadoc
  - archive
  - dockerImage

checkout:
  stage: checkout
  script:
    - echo 'Checking out...'
    - git clone 'https://bitbucket.org/martatrindade/devops-21-22-lmn-1211784'

cache:
  paths:
    - CA5/Alternative/gradle_basic_demo

assemble:
  stage: assemble
  script:
    - echo "Assembling..."
    - ./gradlew assemble
    - echo "Assembled
  artifacts:
    paths:
      - build/libs/*.jar
    expire_in: 1 week
  only:
    - main

test:
  stage: test
  script:
    - echo "Testing..."
    - ./gradlew test
    - echo "Tested"
    artifacts:
      when: always
      reports:
        junit:
          - CA5/Alternative/gradle_basic_demo/build/test-results/test/*.xml

generateJar:
  stage: generateJar
  script:
    - echo "Generating jar..."
    - ./gradlew jar
    - echo "jar generated"

javadoc:
  stage: javadoc
  script:
    - echo "Generating Javadoc..."
    - ./gradlew javadoc
    - echo "Javadoc generated"

archive:
  stage: archive
  script:
    - echo "Archiving..."
  artifacts:
    when: always
    paths:
      - build/distributions/*

build_image:
  stage: dockerImage
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - echo "Generating Docker image..."
    - docker build -t 1211784/my-image:${env.BUILD_ID} .
    - docker push 1211784/my-image:${env.BUILD_ID}
    - echo "Docker image built and pushed"

after_script:
  - echo "End CI"
```


**To potentially achieve a complete solution of the assignment you should also implement the alternative design presented in the previous item 2.**

> **NOTE**: Alternative with GiLabs _**not implemented**_!
> 
> 
> Although GitLabs have a free plan, when trying to create our pipeline on the website, we are requested to add a credit card information,
> therefore the implementation and testing of the prototype `.gitlab-ci.yml` was not done.
>
> To create a pipeline on the website go to GitLabs -> Create project -> CI/CD -> Pipelines
> 
> Warning of the website: User validation required:
> 
>- To use free CI/CD minutes on shared runners, you’ll need to validate your account with a credit card. If you prefer not to provide one, you can run pipelines by bringing your own runners and disabling shared runners for your project. This is required to discourage and reduce abuse on GitLab infrastructure. GitLab will not charge your card, it will only be used for validation
> 
> 
> Alternatively we can Self-host GitLabs by configuring it with a docker image: 
>
>- https://about.gitlab.com/install/
>- Install self-managed GitLab
>- Supported installation methods: In this case we will choose Docker
>- https://docs.gitlab.com/ee/install/docker.html
>- GitLab official Docker image: https://hub.docker.com/r/gitlab/gitlab-ee/
>- `docker pull gitlab/gitlab-ee`
>
> **NOTE**: Implemented Buddy for learning purposes!
> 

**At the end**

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA5/
>- git add Alternative
>- git commit -m "updated README.md CA5 Alternative (resolves #30)"
>- git push
>- git tag ca5-alternative
>- git push origin ca5-alternative
>
