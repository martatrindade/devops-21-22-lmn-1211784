## CA5: Part 2: Alternative Solution - Buddy

After earing the feedback from some colleagues I decided to check out Buddy CI.

This report was made for learning purposes since the deadline for the CA5 was on 06/06/2022, and the alternative chosen was GitLabs 

First go to the website and create an account: https://buddy.works/

- Create new project -> Connect your Bitbucket account...

- Choose the repository: `devops-21-22-lmn-1211784`

- With the repository selected go to -> Pipelines -> Add new pipeline -> New

![img.png](Images/img.png)

- Name: `CA5-Alternative-Buddy`
- Purpose : `CI/CD: Build, test, deploy`
- Trigger : `Manually`
- Branch : `main`

At the end press: Add pipeline

![img_1.png](Images/img_1.png)

A new window will pop-up to Add a new section to our pipeline.

- Since our project uses Gradle we need to add the gradle build tool
- Gradle -> update Environment -> Settings -> Name: Assemble -> Add this action

1. Assemble:
```
cd CA5/Alternative/gradle_basic_demo
chmod u+x gradlew
./gradlew clean assemble
```
- Click the `+` to add new actions to the pipeline
- Repeat the previous step for each action (do not forget to go to settings set the action name)

![img_2.png](Images/img_2.png)
![img_3.png](Images/img_3.png)
![img_4.png](Images/img_4.png)


2. Test
```
cd CA5/Alternative/gradle_basic_demo
./gradlew test
```


3. Generate Jar
```
cd CA5/Alternative/gradle_basic_demo
./gradlew jar
```


4. Javadoc
```
cd CA5/Alternative/gradle_basic_demo
./gradlew javadoc
```


5. Archive
- Add action -> ZIP -> Browse -> Selected Path -> Use this currently opened path
- Provide the target path: CA5/Alternative/gradle_basic_demo/build/distributions/
- Target file name: *.zip
- Settings -> Update name
- Add this action

![img_5.png](Images/img_5.png)
![img_6.png](Images/img_6.png)


6. Build Docker Image
- Add action -> Docker Build Image
- Select the docker file -> Use This docker file
- Settings -> Update name
- Add this action

![img_7.png](Images/img_7.png)


7. Push Docker Image
- Add action -> Docker Push Image
- Docker Hub Account -> Name -> Username: docker -> Password: docker -> Add new integration
- Repository: 1211784/ca5-alternative
- Tag: ca5-alternative
- Settings -> Update name
- Add this action
- 
![img_8.png](Images/img_8.png)


At the end press Run

![img_10.png](Images/img_10.png)
![img_12.png](Images/img_12.png)
![img_13.png](Images/img_13.png)
![img_14.png](Images/img_14.png)
![img_15.png](Images/img_15.png)

Export yaml file

- Go to Pipeline -> Press the 3 dots -> Download YAML config

![img_16.png](Images/img_16.png)

```yaml
- pipeline: "CA5-Alternative-Buddy"
  on: "CLICK"
  refs:
  - "refs/heads/main"
  priority: "NORMAL"
  fail_on_prepare_env_warning: true
  actions:
  - action: "Assemble"
    type: "BUILD"
    working_directory: "/buddy/devops-21-22-lmn-1211784"
    docker_image_name: "library/gradle"
    docker_image_tag: "5.4-jdk8"
    execute_commands:
    - "cd CA5/Alternative/gradle_basic_demo"
    - "chmod u+x gradlew"
    - "./gradlew clean assemble"
    volume_mappings:
    - "/:/buddy/devops-21-22-lmn-1211784"
    cache_base_image: true
    shell: "BASH"
  - action: "Test"
    type: "BUILD"
    working_directory: "/buddy/devops-21-22-lmn-1211784"
    docker_image_name: "library/gradle"
    docker_image_tag: "5.4-jdk8"
    execute_commands:
    - "cd CA5/Alternative/gradle_basic_demo"
    - "./gradlew test"
    volume_mappings:
    - "/:/buddy/devops-21-22-lmn-1211784"
    cache_base_image: true
    shell: "BASH"
  - action: "Generate Jar"
    type: "BUILD"
    working_directory: "/buddy/devops-21-22-lmn-1211784"
    docker_image_name: "library/gradle"
    docker_image_tag: "5.4-jdk8"
    execute_commands:
    - "cd CA5/Alternative/gradle_basic_demo"
    - "./gradlew jar"
    volume_mappings:
    - "/:/buddy/devops-21-22-lmn-1211784"
    cache_base_image: true
    shell: "BASH"
  - action: "Javadoc"
    type: "BUILD"
    working_directory: "/buddy/devops-21-22-lmn-1211784"
    docker_image_name: "library/gradle"
    docker_image_tag: "5.4-jdk8"
    execute_commands:
    - "cd CA5/Alternative/gradle_basic_demo"
    - "./gradlew javadoc"
    volume_mappings:
    - "/:/buddy/devops-21-22-lmn-1211784"
    cache_base_image: true
    shell: "BASH"
  - action: "Archive directory"
    type: "ZIP"
    local_path: "CA5/Alternative/gradle_basic_demo/build/libs/basic_demo-0.1.0.jar"
    destination: "CA5/Alternative/gradle_basic_demo/build/distributions/*.zip"
    deployment_excludes:
    - ".git"
  - action: "Build Docker image"
    type: "DOCKERFILE"
    dockerfile_path: "CA5/Alternative/gradle_basic_demo/Dockerfile"
    target_platform: "linux/amd64"
  - action: "Push Docker image"
    type: "DOCKER_PUSH"
    docker_image_tag: "ca5-alternative"
    repository: "1211784/ca5-alternative"
    integration_hash: "NDm8VvJ4open7o1la7Az1XPYRr"

```

## Conclusion

Comparing to Jenkins, Buddy has a more user-friendly interface and it's easier to use.

While creating our pipeline there were no issues, but the feedback provided by some colleagues is that, sometimes, the automatic commands runned by Buddy doesn't allow the user to decide how some commands should be run.
