package basic_demo;
import org.junit.Test;
import static org.junit.Assert.*;

public class AppTest {
    @Test
    public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }

    @Test
    public void equalValues() {
        int expected = 1;
        int result = 1;
        assertEquals(expected, result);
    }

}
