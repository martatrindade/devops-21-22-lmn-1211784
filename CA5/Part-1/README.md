## CA5: Part 1: Goals/Requirements

> Before start make a pull to check if repository is up to date
>
>- git pull
>
> Create CA5 folder and Part-1 folder
>
>- mkdir CA5
>- mkdir CA5/Part-1
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA5/Part-1
>- echo “CA5: First Week” >> README.md
>
> To make changes on the file use the command:
>
>- nano README.md
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784
>- git add CA5
>- git commit -m "created new directory CA5 Part-1 (addresses #28)"
>- git push
>

**The goal of the Part 1 of this assignment is to practice with Jenkins using the "gradle basic demo" project that should already be present in the student’s individual repository.**

**1. The goal is to create a very simple pipeline (similar to the example from the lectures).**

- Note that this project should already be present in a folder inside your individual repository! (CA2 Part-1)
- When configuring the pipeline, in the Script Path field, you should specify the relative path (inside your repository) for the Jenkinsfile. For instance, ’ca2/part1/gradle-basic-demo/Jenkinsfile’.

> Setup Jenkins with Docker
>
>- In this case the option https://hub.docker.com/r/jenkinsci/blueocean/ (see command bellow) was not working properly
>
>  * `docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean`
>

> Instead, let’s download the .war file
>
>- First download Jenkins: https://www.jenkins.io/download/
>- https://www.jenkins.io/doc/book/installing/war-file/
>- Download the latest stable Jenkins LTS WAR file: **Generic Java Package (.war)**
>- On the folder where the .war file is located run the command:
>- `java -jar jenkins.war`
>- Since JDK17 and JDK11 are installed on the machine, the command to run is: `java -jar jenkins.war --enable-future-java`
>- For other error try: https://www.happycoders.eu/java/how-to-switch-multiple-java-versions-windows/
>- If the port is busy run the command: `java -jar jenkins.war --enable-future-java --httpPort=8084`
>- Open `http://localhost:8084` to access Jenkins
>- In the terminal a password will be provided (98ca2d47849a42c1bcbfc55fc8b7f559)
>- Run the initial setup wizard with "recommended plugins"
>- If we need to add plugins go to: Manage Jenkins -> Manage Plugins
>- Global Tool Configuration option is useful if we need to change some configurations, but it’s not a common thing to do.
>- Now let’s create a pipeline:
>- Go to Jenkins -> Dashboard -> New Item -> Name: ca5-part1 -> Pipeline -> Ok
>- Insert a test script:
>
```java
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'Building...'
                script {
                    if (isUnix())
                        sh 'java -version'
                    else
                        bat 'java -version'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
            }
        }
    }
}
```
>
>- Save
>- Build Now 
>

**2. You should define the following stages in your pipeline:**

- **Checkout.** To checkout the code form the repository
- **Assemble**. Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)!
- **Test**. Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results.

  * Do not forget to add some unit tests to the project (maybe you already have done it).

- **Archive**. Archives in Jenkins the archive files (generated during Assemble)

> Now let’s create the pipeline for this exercise:
> 
>- Go to Jenkins -> Dashboard -> New Item -> Name: ca5-part1-ex2 -> Pipeline -> Ok
>- Copy CA2/Part-1/gradle_basic_demo to CA5/Part-1
>- `cd D:/SWitCH/devops-21-22-lmn-1211784/CA5/Part-1/gradle_basic_demo`
>- Create `Jenkinsfile`:
>
```java
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'main', url: 'https://bitbucket.org/martatrindade/devops-21-22-lmn-1211784'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA5/Part-1/gradle_basic_demo') {
                    script {
                        if (isUnix())
                            sh './gradlew clean assemble'
                        else
                            bat './gradlew clean assemble'
                    }
                }
                echo 'Assembled'
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA5/Part-1/gradle_basic_demo') {
                    script {
                        if (isUnix())
                            sh './gradlew test'
                        else
                            bat './gradlew test'
                    }
               }
                echo 'Tested'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA5/Part-1/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*'
                }
            }
        }
    }

    post {
        always {
            junit allowEmptyResults: true, testResults: 'CA5/Part-1/gradle_basic_demo/build/test-results/test/*.xml'
        }
    }
}
```
>
> If the .jar file is not pushed use the following command to force:
> 
>- `git add -f CA5/Part-2/gradle_basic_demo/gradle/wrapper/gradle-wrapper.jar`
>- `git add -f CA5/Part-2/gradle_basic_demo/buil/libs/basic_demo-0.1.0.jar`
>
> Add a unit test: CA5/Part-1/grade_basic_demo/src/test/java/basic_demo/AppTest.java
>
```java
@Test
    public void equalValues() {
        int expected = 1;
        int result = 1;
        assertEquals(expected, result);
    }
```
>
> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA5/
>- git add Part-1
>- git commit -m "updated CA5 Part-1 (resolves #28)"
>- git push
>
>
> Setup Jenkins Job
> 
>- On the: http://localhost:8084/job/ca5-part1-ex2/configure
>- Pipeline -> Definition: Pipeline script from SCM
>- SCM: Git
>- Repository URL: https://bitbucket.org/martatrindade/devops-21-22-lmn-1211784
>- Credentials: needed in case the repository is private
>
>  * To create a credential, go to: Manage Jenkins -> Manage Credentials -> Store scoped: Jenkins -> Global credentials -> Add Credentials (left panel) -> SSH Username with private key -> CredentialsID: public name to insert in the Jenkinsfile -> Private key (generated with the command sssh-keygen) -> Add
>
>- Branch: `*/main`
>- Script path: `CA5/Part-1/gradle_basic_demo/Jenkinsfile`
>- Save
>- Build Now
>- If the Logs are successful but there’s an error “No test report files were found”, add the following to the Jenkinsfile Post: `allowEmptyResults: true, testResults:`
>

![img.png](img.png)

_**Automate Jenkins Build**_

>- To automate the Built from Jenkins do the following:
>- Jenkins -> Manage Jenkins -> Manage Plugins -> Available: Bitbucket Push and Pull Request -> Select check box -> Install without restart (bottom of the page)
>- https://plugins.jenkins.io/bitbucket-push-and-pull-request/
>- On Bitbucket -> Repository settings -> Workflow: Webhooks -> new
>- https://bitbucket.org/martatrindade/devops-21-22-lmn-1211784/admin/webhooks/new
>- On Jenkins -> Manage Jenkins -> Configure System -> Jenkins Location -> Jenkins URL: `http://104.192.136.2:8084/` -> Save
>- See a list of valid Ips: https://support.atlassian.com/bitbucket-cloud/docs/what-are-the-bitbucket-cloud-ip-addresses-i-should-use-to-configure-my-corporate-firewall/
>- Troubleshooting for error 403: https://community.atlassian.com/t5/Bitbucket-questions/Not-able-to-add-webhook-with-Jenkins-URL-for-triggering-CI-build/qaq-p/1448230
>- URL (This is the Jenkins URL where plugin is installed): http://localhost:8084/job/ca5-part1-ex2/bitbucket-hook/
>

**3. At the end of the part 1 of this assignment mark your repository with the tag ca5-part1.**

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA5/
>- git add Part-1
>- git commit -m "updated README.md CA5 Part-1 (resolves #28)"
>- git push
>- git tag ca5-part1
>- git push origin ca5-part1
>
