pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'main', url: 'https://bitbucket.org/martatrindade/devops-21-22-lmn-1211784'
            }
        }

        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    script {
                        if (isUnix())
                            sh './gradlew clean assemble'
                        else
                            bat './gradlew clean assemble'
                    }
                }
                echo 'Assembled'
            }
        }

        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    script {
                        if (isUnix())
                            sh './gradlew test'
                        else
                            bat './gradlew test'
                    }
               }
                echo 'Tested'
            }
        }

        stage('Generate Jar') {
            steps {
                echo 'Generating jar...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    bat './gradlew jar'
                }
                echo 'Jar generated'
            }
        }

        stage('Javadoc') {
            steps {
                echo 'Creating Javadoc...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    bat './gradlew javadoc'
                }
                echo 'Javadoc created'
            }
        }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*'
                }
                echo 'Archived'
            }
        }

        stage ('Docker Image') {
            steps {
                echo 'Generating Docker image...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    script {
                        docker.withRegistry('https://registry.hub.docker.com', 'dockerhub_credentials') {
                            def customImage = docker.build("1211784/my-image:${env.BUILD_ID}")
                            customImage.push()
                        }
                    }
                }
                echo 'Docker image generated'
            }
        }

    }

    post {
        always {
            junit allowEmptyResults: true, testResults:'CA5/Part-2/gradle_basic_demo/build/test-results/test/*.xml'
            publishHTML([allowMissing: false,
                alwaysLinkToLastBuild: false,
                keepAll: false, reportDir: 'CA5/Part-2/gradle_basic_demo/build/docs/javadoc',
                reportFiles: 'index.html',
                reportName: 'HTML Report',
                reportTitles: 'The Report'])
        }
    }
}