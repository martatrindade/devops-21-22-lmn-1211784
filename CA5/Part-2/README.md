## CA5: Part 2: Goals/Requirements

> Before start make a pull to check if repository is up to date
>
>- git pull
>
> Create CA5 folder and Part-2 folder
>
>- mkdir CA5/Part-2
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA5/Part-2
>- echo “CA5: Second Week” >> README.md
>
> To make changes on the file use the command:
>
>- nano README.md
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784
>- git add CA5
>- git commit -m "created new directory CA5 Part-2 (addresses #29)"
>- git push
>

**The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2)**

> Copy the application to CA5/Part-2
>
>- `cp D:/SWitCH/devops-21-22-lmn-1211784\CA5\Part-1\gradle_basic_demo D:/SWitCH/devops-21-22-lmn-1211784\CA5\Part-2`
>
> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA5/
>- git add Part-2
>- git commit -m " updated CA5 Part-2 added application folder (addresses #29)"
>- git push
>
> Note: Do not forget to use the command `git add -f .` on the `build` and `gradle` folder!
>

> Troubleshoot Jenkins:
>
>- If the system has other JDK versions follow the following link to setup and run java 11 as default
>- https://www.happycoders.eu/java/how-to-switch-multiple-java-versions-windows/
>- Use **COMMAND PROMPT** and run the command: `java --version`
>- To change the version to java 11 type: `java11.bat`
>
> Setup Jenkins:
>
>- Go to the folder where the .war file is located:
>- `D:`
>- `cd .D:\Downloads`
>- On the folder where the .war file is located run the command:
>- `java -jar jenkins.war`
>- If the port is busy run the command: `java -jar jenkins.war --httpPort=8084`
>- On the browser open: http://localhost:8084/
>
> Install Plugins:
>
>- Manage Jenkins -> Manage Plugins -> Available ->
>- HTML Publisher
>- Docker Pipeline
>

You should define the following stages in your pipeline:

- **Checkout.** To checkout the code from the repository

- **Assemble.** Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)!

- **Test.** Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results.

  * Do not forget to add some unit tests to the project (maybe you already have done it).

- **Javadoc.** Generates the javadoc of the project and publish it in Jenkins. See the publishHTML step for further information on how to archive/publish html reports.

- **Archive.** Archives in Jenkins the archive files (generated during Assemble, i.e., the war file)

- **Publish Image.** Generate a docker image with Tomcat and the war file and publish it in the Docker Hub.

  * See https://jenkins.io/doc/book/pipeline/docker/, section Building Containers.
  * See also the next slide for an example. In the example, docker.build will build an image from a Dockerfile in the same folder as the Jenkinsfile. The tag for the image will be the job build number of Jenkins.

> Update Jenkinsfile:
```java
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git branch: 'main', url: 'https://bitbucket.org/martatrindade/devops-21-22-lmn-1211784'
            }
        }

        stage('Assemble') {
            steps {
                echo 'Assembling...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    script {
                        if (isUnix())
                            sh './gradlew clean assemble'
                        else
                            bat './gradlew clean assemble'
                    }
                }
                echo 'Assembled'
            }
        }

        stage('Test') {
            steps {
                echo 'Testing...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    script {
                        if (isUnix())
                            sh './gradlew test'
                        else
                            bat './gradlew test'
                    }
               }
                echo 'Tested'
            }
        }

        stage('Generate Jar') {
            steps {
                echo 'Generating jar...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    bat './gradlew jar'
                }
                echo 'Jar generated'
            }
        }

        stage('Javadoc') {
            steps {
                echo 'Creating Javadoc...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    bat './gradlew javadoc'
                }
                echo 'Javadoc created'
            }
        }

        stage('Archiving') {
            steps {
                echo 'Archiving...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    archiveArtifacts 'build/distributions/*'
                }
                echo 'Archived'
            }
        }

        stage ('Docker Image') {
            steps {
                echo 'Generating Docker image...'
                dir('CA5/Part-2/gradle_basic_demo') {
                    script {
                        docker.withRegistry('https://registry.hub.docker.com', 'dockerhub_credentials') {
                            def customImage = docker.build("1211784/my-image:${env.BUILD_ID}")
                            customImage.push()
                        }
                    }
                }
                echo 'Docker image generated'
            }
        }

    }

    post {
        always {
            junit allowEmptyResults: true, testResults:'CA5/Part-2/gradle_basic_demo/build/test-results/test/*.xml'
            publishHTML([allowMissing: false,
                alwaysLinkToLastBuild: false,
                keepAll: false, reportDir: 'CA5/Part-2/gradle_basic_demo/build/docs/javadoc',
                reportFiles: 'index.html',
                reportName: 'HTML Report',
                reportTitles: 'The Report'])
        }
    }
}
```
>
>- Note: Docker Image name: [user]/[image_name]:[version]
>
> Generate credentials for DockerHub connection
>
>- Manage Jenkins -> Manage credentials -> Global -> Add Credentials
>- Kind: Username with password
>- Scope: Global
>- Username: (from docker hub)
>- Password: (from docker hub)
>- ID: dockerhub_credentials
>- Ok
 
> Create Dockerfile (use the file from CA4/Part-1/Version-2):
```java
# Download base image ubuntu 18.04
FROM ubuntu:18.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Install java
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

# Copy the jar file into the container
COPY build/libs/basic_demo-0.1.0.jar .

# Expose Port for the Application
EXPOSE 59001

# Run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```
>
>
> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA5/
>- git add Part-2
>- git commit -m "updated CA5 Part-2 added Dockerfile and Jenkinsfile (addresses #29)"
>- git push
>

> Let’s create the pipeline for this exercise:
>
>- Go to Jenkins -> Dashboard -> New Item -> Name: ca5-part2 -> Pipeline -> Ok
>
> Setup Jenkins Job
>
>- On the: http://localhost:8084/job/ca5-part1-ex2/configure
>- Pipeline -> Definition: Pipeline script from SCM
>- SCM: `Git`
>- Repository URL: `https://bitbucket.org/martatrindade/devops-21-22-lmn-1211784`
>- Branch: `*/main`
>- Script path: `CA5/Part-2/gradle_basic_demo/Jenkinsfile`
>- Save
>- Build now

 ![img.png](img.png)

**2. At the end of the part 2 of this assignment mark your repository with the tag ca5-part2**

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA5/
>- git add Part-2
>- git commit -m "updated README.md CA5 Part-2 (resolves #29)"
>- git push
>- git tag ca5-part2
>- git push origin ca5-part2
>
