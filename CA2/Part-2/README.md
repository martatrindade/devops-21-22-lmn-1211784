## CA2: Part 2: Goals/Requirements

**1. In your repository create a new branch called tut-basic-gradle. You should use this branch for this part of the assignment (do not forget to checkout this branch).**

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/
>
> Before start make a pull to check if repository is up to date
>
>- git pull
>
> Create CA2 Part-2 folder
>
>- mkdir CA2/Part-2
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2
>- echo “CA2: Second Week” >> README.md
>
>To make changes on the file use the command:
>- nano README.md
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "created new directory CA2 Part-2 (Addresses #12)"
>- git push
>
> Create the new branch
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git branch tut-basic-gradle
>- git checkout tut-basic-gradle
>

**2. As instructed in the readme file of the tutorial, use https://start.spring.io to start a new gradle spring boot project with the following dependencies: Rest Repositories; Thymeleaf; JPA; H2.**

> Go to the website: https://start.spring.io
>
>-  Project: Gradle Project
>-  Language: Java
>- Spring Boot: 2.6.6
>- Packaging: Jar
>- Java: 11
>

**3. Extract the generated zip file inside the folder "CA2/Part2/" of your repository. We now have an "empty" spring application that can be built using gradle. You can check the available gradle tasks by executing ./gradlew tasks.**

> Extract the “demo” folder inside D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2
>
> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Run the tasks:
>
>- `./gradlew tasks`
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 closes #13)"
>- git push -u origin tut-basic-gradle
>

**4. Delete the src folder. We want to use the code from the basic tutorial...**

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Delete the src folder
>- rm -rf src/
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 closes #14)"
>- git push -u origin tut-basic-gradle
>

**5. Copy the src folder (and all its subfolders) from the basic folder of the tutorial into this new folder.**

> Copy CA1/tut-react-and-spring-data-rest/basic/src folder to CA2/Part-2 folder
>
>- cp -R /d/SWitCH/devops-21-22-lmn-1211784/CA1/tut-react-and-spring-data-rest/basic/src /d/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "created new directory CA2 and Part-2 (Addresses #12 closes #15)"
>- git push -u origin tut-basic-gradle
>

* Copy also the files webpack.config.js and package.json

> Copy webpack.config.js from CA1/…/basic
>
>- cp -R /d/SWitCH/devops-21-22-lmn-1211784/CA1/tut-react-and-spring-data-rest/basic/webpack.config.js /d/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Copy package.json from CA1/…/basic
>
>- cp -R /d/SWitCH/devops-21-22-lmn-1211784/CA1/tut-react-and-spring-data-rest/basic/package.json /d/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 reopen #15)"
>- git push -u origin tut-basic-gradle
>

* Delete the folder src/main/resources/static/built/ since this folder should be generated from the javascrit by the webpack tool.

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo/src/main/resources/static/
>
> Delete the built folder
>- rm -rf built/
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 closes #15)"
>- git push -u origin tut-basic-gradle
>


**6 You can now experiment with the application by using `./gradlew bootRun`. 

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Run the tasks:
>
>- `./gradlew bootRun`
>

* Notice that the web page http://localhost:8080 is empty! This is because gradle is missing the plugin for dealing with the frontend code!

> Press CTRL+C to stop executing the command
>

**7. We will add the gradle plugin org.siouan.frontend to the project so that gradle is also able to manage the frontend. See https://github.com/Siouan/frontend-gradle-plugin**

> Go to the website: https://github.com/Siouan/frontend-gradle-plugin
>

* This plugin is similar to the one used in the Maven Project (frontend-maven-plugin). See https://github.com/eirslett/frontend-maven-plugin

> Go to the website: https://github.com/eirslett/frontend-maven-plugin
>

**8. Add one of the following lines to the plugins block in build.gradle (select the line according to your version of java: 8 or 11):**

> Edit the file build.gradle and add the jdk plugin
>
```java
plugins {
	id 'org.springframework.boot' version '2.6.6'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
	id "org.siouan.frontend-jdk11" version "6.0.0"
}
```
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 closes #16)"
>- git push -u origin tut-basic-gradle
>

**9. Add also the following code in build.gradle to configure the previous plugin:**

> Edit the file build.gradle to configure the plugin
>
```java
frontend {
nodeVersion = "14.17.3"
assembleScript = "run build"
cleanScript = "run clean"
checkScript = "run check"
}
```

**10. Update the scripts section/object in package.json to configure the execution of webpack:**

> Edit the file package.json and replace the script section
>
```java
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
},
```
> Notice that the main/js folder and resources/static folder is updated
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 closes #17)"
>- git push -u origin tut-basic-gradle
>

**11. You can now execute `./gradlew build`. The tasks related to the frontend are also executed and the frontend code is generated.**

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Run the tasks:
>
>- `./gradlew build`
>

**12. You may now execute the application by using `./gradlew bootRun`**

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Run the application:
>
>- `./gradlew bootRun`
>
> See if the application is running on the browser
>
> http://localhost:8080
>
> Press CTRL+C to stop executing the application
>

**13. Add a task to gradle to copy the generated jar to a folder named "dist" located at the project root folder level**

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Create the dist folder
>
>- mkdir dist
>
> Edit the file build.gradle and add the new task
>
```java
task copyJarToDist(type: Copy) {
	from 'build/libs'
	into 'dist'
}
```
> Run the new copy task:
>
>- `./gradlew copyJarToDist`
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 closes #18)"
>- git push -u origin tut-basic-gradle

**14. Add a task to gradle to delete all the files generated by webpack (usually located at src/resources/main/static/built/). This new task should be executed automatically by gradle before the task clean.**

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Edit the file build.gradle and add the new delete task that runs automatically before the **clean** task
```java
clean.doFirst {
	delete 'src/main/resources/static/built'
}
```
> Run the clean task:
>
>- `./gradlew clean`
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Part-2
>- git commit -m "CA2 and Part-2 (Addresses #12 closes #19)"
>- git push -u origin tut-basic-gradle
>

> An alternative to the `clean.doFirst` is the following task:
```java
task deleteWebpack(type: Delete) {
   delete 'src/main/resources/static/built'
   followSymlinks = true
}

clean.dependsOn deleteWebpack
```

**15. Experiment all the developed features and, when you feel confident that they all work commit your code and merge with the master branch.**

> Try running the build again and check if the previous webpack files are restored
>
> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Part-2/demo
>
> Run the tasks:
>
>- `./gradlew build`
>

> Merge with the main branch
>
>- git checkout main
>- git merge tut-basic-gradle
>- git push
>
> To delete the branch use the command:
>
>-  git branch -d tut-basic-gradle
>

**16. Document all your work in the readme.md file**

> Update the CA2/Part-2/readme.md file
>
> To make changes on the file on the terminal use the command:
>- nano README.md
>
> The file can also be updated through IntellIJ / VSCode / …
>

**17. At the end of part 2 of this assignment mark your repository with the tag ca2-part2.**

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA2/
>- git add Part-2
>- git commit -m "CA2 and Part-2 (resolves #12)"
>- git push
>- git tag ca2-part2
>- git push origin ca2-part2
>

## CA2: Part 2 Alternative solved on a different folder and README.md