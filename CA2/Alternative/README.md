## CA2: Part 2: Alternative Solution

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/
>
> Create CA2 Alternative folder
>
>- mkdir CA2/Alternative
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Alternative
>- echo “CA2: Second Week” >> README.md
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Alternative
>- git commit -m "created new directory for CA2 Alternative (Addresses #20)"
>- git push
>

**1. Present how the alternative tool compares to Gradle regarding build automation features**

* In particular, compare how the build tool can be extended with new functionality (i.e., new plugins or new tasks)

> There are a few alternatives to **Gradle**, during the theoretical classes we talked about the two most common ones, **Maven** and **Ant**
>

> **GRADLE**
>
>- Gradle is a open-source dependency management and build automation tool, released in 2012. It follows the principle of “Convention over configuration”
>- It combines the good parts of Maven and Ant and builds on top of them. It adopted Ant flexibility and Maven life cycle. 
>- Supports Maven and Ivy repositories for retrieving the dependencies.
>- Gradle files are written in DSL (Domains Specific Language) based on Groovy, instead of XML, on a file usually called “build.gradle”
>- The file contains: Plugins, Repositories, Configurations, Dependencies, Tasks, …

>**MAVEN**
>
>- Maven is a dependency management and a build automation tool, released in 2004. It follows the principle of “Convention over configuration”, relies on conventions and provides predefined commands (goals).
>- Maven files are written in XML usually called “pom.xml”
>- The file contains: Properties, Dependencies, Build
>- Example:
```java
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>switch2021</groupId>
    <artifactId>switchproject</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <junit.jupiter.version>5.8.1</junit.jupiter.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

    <build>
        <plugins>
            <!-- Required for compiling the project usign maven -->
            <plugin><!-- Compiler configuration-->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>

                    <showWarnings>false</showWarnings>   <!-- Needs this -->

                    <compilerArgs>

                    </compilerArgs>

                    <encoding>${project.build.sourceEncoding}</encoding>

                </configuration>
            </plugin>
    </build>
</project>
```

> **ANT**
>
>- Apache ANT (Another Neat Tool) is an open source project by Apache, released in the year 2000, used for automating the build processes for java and non-java applications. It follows the principle of “Configuration over convention”.
>- ANT files are written in XML usually called “build.xml”
>- The file contains three elements: Project, Target and Task
>- Example:
```java
<?xml version=”1.0"?>
<project name="the project" default="compile" basedir="." xmlns:ivy="antlib:org.apache.ivy.ant">

	<property name="src.dir" location="src" />
	<property name="bin.dir" location="bin" />
	<property name="lib.dir" location="lib" />

	<path id="classpath">
		<fileset dir="${lib.dir}" includes="**/*.jar" />
	</path>

<target name=”hello”>
<echo>Hello, World</echo> 
</target>

<!-- Target for deleting the existing directories -->
  <target name="clean">
<delete dir="${build.dir}" />
  </target>
 
  <!-- Target for creating the new directories -->
  <target name="makedir">
<mkdir dir="${build.dir}" />
  </target>

<!-- Target to add dependencies (ivy required) -->
<target name="update-lib-dir">
    <property name="ivy.install.version" value="2.4.0"/>
    <property name="ivy.jar.file" value="lib-ant/ivy-${ivy.install.version}.jar"/>
    <property name="lib-ivy.dir" value="lib-ivy"/>
    <get src="https://repo1.maven.org/maven2/org/apache/ivy/ivy/${ivy.install.version}/ivy-${ivy.install.version}.jar"
        dest="${ivy.jar.file}" usetimestamp="true" />

    <taskdef resource="org/apache/ivy/ant/antlib.xml">
        <classpath>
            <pathelement location="${ivy.jar.file}"/>
        </classpath>
    </taskdef>

    <!-- define your destination directory here -->
    <retrieve pattern="${lib-ivy.dir}/[type]/[artifact]-[revision].[ext]" sync="true">
        <!-- Add all your maven dependencies here -->
        <dependency org="com.twelvemonkeys.imageio" name="imageio-jpeg" rev="3.4.2"/>
        <dependency org="com.twelvemonkeys.imageio" name="imageio-tiff" rev="3.4.2"/>
    </retrieve>
</target>
</project>
```

> In short the table bellow represents the differences:
>
>
> | Build tool     | Gralde                 | Maven              | Ant         |
> | -------------- |------------------------|--------------------|-------------|
> | Language       | DSL                    | XML               | XML         |
> | Built file     | build.gradle           | pom.xml           | build.xml   |
> | Contents       | Plugins, Repositories, Configurations, Dependencies, Tasks, …          | Properties, Dependencies, Build          | Project, Target and Task         |


**2. Describe how the alternative tool could be used (i.e., only the design of the solution) to solve the same goals as presented for this assignment (see previous slides)**

> In this case the alternative chosen to implement CA2/Part-2 is Maven.
>
>- To make the same implementation made with gradle first we need to download a new base spring project that uses maven from  https://start.spring.io.
>- To add the frontend dependencies, we will have to use another plugin, since the other one is specific for gradle projects. In this case we will use the link provided as a Maven alternative during the classes from https://github.com/eirslett/frontend-maven-plugin
>- To create the new tasks we need to edit the `pom.xml` file and add them inside the section <build> <plugin></plugin> </build>
>- To run the application the commands are: `./mvnw spring-boot:run`, `./mvnw install` and `./mvnw clean`


**To potentially achieve a complete solution of the assignment you should also implement the alternative design presented in the previous item 2.**

> For the implementation first we need to download a new maven spring application and follow the same steps as CA2 Part-2.
> 
> **Step 2 and 3**
> 
> Go to the website: https://start.spring.io
> and download a spring application using maven
>
> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Alternative/demo
>
> Run maven tasks
>
>- `./mvnm spring-boot:<task_name>`
>

> **Step 4, 5 and 6**
>
> Copy files from CA1
>
>- src
>- webpack.config.js
>- package.json
>
> Delete the folder: src/main/resources/static/built
>
> Run the application (inside the alternative/demo directory)
>
>- `./mvnw spring-boot:run`
>

> **Step 7, 8 and 9**
>
> Go to the website: https://github.com/eirslett/frontend-maven-plugin
> to get a similar alternative to gradle
>
> Edit the file **pom.xml** and add the maven plugin to the build section
```java
<plugin>
    <groupId>com.github.eirslett</groupId>
    <artifactId>frontend-maven-plugin</artifactId>
    <!-- Use the latest released version:
    https://repo1.maven.org/maven2/com/github/eirslett/frontend-maven-plugin/ -->
    <version>1.12.1</version>
    <executions>

        <execution>
            <id>install node and npm</id>
            <goals>
                <goal>install-node-and-npm</goal>
            </goals>
            <configuration>
                <!-- See https://nodejs.org/en/download/ for latest node and npm (lts) versions -->
                <nodeVersion>v8.11.1</nodeVersion>
                <npmVersion>5.6.0</npmVersion>
            </configuration>
        </execution>

        <execution>
            <id>npm install</id>
            <goals>
                <goal>npm</goal>
            </goals>
            <!-- Optional configuration which provides for running any npm command -->
            <configuration>
                <arguments>install</arguments>
            </configuration>
        </execution>

        <execution>
            <id>npm run build</id>
            <goals>
                <goal>npm</goal>
            </goals>
            <configuration>
                <arguments>run build</arguments>
            </configuration>
        </execution>

    </executions>
</plugin>
```

> **Step 10**
> Edit the file package.json and replace the script section
>
```java
"scripts": {
"webpack": "webpack",
"build": "npm run webpack",
"check": "echo Checking frontend",
"clean": "echo Cleaning frontend",
"lint": "echo Linting frontend",
"test": "echo Testing frontend"
},
```

> **Step 11 and 12**
> Run the application (inside the alternative/demo directory)
>
>- `./mvnw spring-boot:run`

> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Alternative
>- git commit -m "CA2 Alternative - Part 2 Steps 2 to 12 (Addresses #20)"
>- git push
>

> **Step 13**
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2/Alternative/demo
>- mkdir dist
>- Edit the file **pom.xml** and add the new task
> 
> Note: in maven the files are located on the folder demo/target.
```java
<!-- Step 13 create copy task -->
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-resources-plugin</artifactId>
    <version>3.2.0</version>
    <executions>
        <execution>
            <id>copy-jar</id>
            <phase>generate-sources</phase>
            <goals>
                <goal>copy-resources</goal>
            </goals>
                <configuration>
                    <outputDirectory>dist</outputDirectory>
                    <resources>
                        <resource>
                        <directory>target</directory>
                            <includes>
                                <include>**/*.jar</include>
                            </includes>
                        </resource>
                    </resources>
                </configuration>
        </execution>
    </executions>
</plugin>
```
> Run the new copy task:
>
>- `./mvnw spring-boot:run` or `./mvnw install`
>

> **Step 14**
```java
<!-- Step 14 create deleteWebpack task -->
<plugin>
	<artifactId>maven-clean-plugin</artifactId>
	<version>3.2.0</version>
	<configuration>
		<filesets>
			<fileset>
				<directory>src/main/resources/static/built</directory>
				<followSymlinks>false</followSymlinks>
			</fileset>
		</filesets>
	</configuration>
</plugin>
```
> Run the new delete task:
>
>- `./mvnw clean`
>

> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA2
>- git add Alternative
>- git commit -m "CA2 Alternative step 14 from Part-2 (Addresses #20)"
>- git push
>


## At the end of the alternative update README.md

> Update README.md commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA2/
>- git add Part-2
>- git commit -m "CA2 Alternative (resolves #20)"
>- git push
>- git tag ca2-alternative
>- git push origin ca2-alternative

