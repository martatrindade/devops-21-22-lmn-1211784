## CA4: Part 2: Alternative Solution

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/
>
> Create CA4 Alternative folder
>
>- mkdir CA4/Alternative
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Alternative
>- echo “CA4: Alternative” >> README.md
>
> To make changes on the file use the command:
>
>- nano README.md
>
> Commit the changes and push
>
>- git add CA4
>- git commit -m "created new directory CA4 Alternative (addresses #27)"
>- git push
>

### For the alternative select one option:

> For the alternative the options are:
>
>- Option 1: Explore Kubernetes
>- Option 2: Explore how to deploy the solution to Heroku
>
> The selected alternative presented in this report will be **Option 1 - Kubernetes**.
>

**Option 1 - Explore Kubernetes. See https://kubernetes.io.**

- Be aware that Kubernetes can be an alternative or a complement to Docker.

> First we need to understand what is docker and Kubernetes.
>
> **DOCKER**
>
>- Docker is open-source technology consisting on a suite of software development tools for creating, sharing and running individual containers at both small and large scale, as a way to package and run applications.
>- Docker is an easy to use container technology for developers thanks to tools like the **Docker Engine** (main command line tool that creates and runs containers), **Docker Container Image Format** (for creating and sharing container images), **Dockerfile** (a language for building container images), **Docker Hub** (an online registry for publishing and sharing container images on the web), **Docker Compose** (a light tool to share container setup instructions), and **Docker Swarm Mode** (a  tool for managing containers running on multiple servers). 
>- With Docker it is possible to create container images, run your own container images, share images with co-workers, share images on the internet, run third-party containers and run multi-container applications.
>
> 
> **KUBERNETS**
>
>- Kubernetes, often abbreviated as “k8s” or “k-eights”, is an open-source container orchestration toolset that provides an application programming interface (API) for managing, automating, and scaling containerized applications
>- Tool which manages (“orchestrates”) container-based applications running on a cluster of servers.
>- Tool for deploying and managing containers on your servers, or in the cloud.
>- Kubernetes is very extensible and automates a ton of common operations.
>- With Kubernetes it is possible to make workloads portable, since container apps are separate from their infrastructure, it is also possible to scale containers easily by defining complex containerized applications and deploying them across a cluster of servers or even multiple clusters. 
>- Finally, since it has a large open-source community of developers and companies there’s a frequent release/publication of new extensions and plugins that add capabilities such as security, monitoring, and management to Kubernetes.
>- The main features of Kubernetes include, Container scheduling, container management, auto-scaling containers, networking, storage, Logs and monitoring, security, not forgetting the extensibility of the toolset.
>- Deploying your own, or third-party, container-based applications, as well as connecting your apps to each other, upgrade applications and gather metrics on your apps are some of the actions you can do with Kubernetes.
>
>
> **SUMMARY**
>
>- Docker is a tool to help you to create and run containers. Kubernetes is a tool for managing them while you sleep.
>- Docker and Kubernetes aren’t competitors, they’re two technologies which complement each other.
>- We can use Docker without Kubernetes, and Kubernetes without Docker.
>- Due to its large community, Kubernetes is an on growing tool with margin to improvement via the add-ons and plugins, developed by those who want to upgrade the tool or adjust it to their own needs.
>- Using Docker alongside with Kubernetes can make your infrastructure more robust and your app more highly available (keeping the app online, even if some nodes stay or go offline) and can make your application more scalable.


- Present how the alternative tool compares to Docker regarding containerization features;

>- While Docker Compose can only run containers on a single host machine, Kubernetes is used to run containers of several virtual or real computers.
>- As stated by some authors Docker is a great tool to learn how to build and run containers on your laptop, while Kubernetes serves as the main tool to orchestrate your containers and run them in production.
>- Docker is still a useful tool for building containers, and the images that result from running `docker build` can still run in Kubernetes cluster. (https://kubernetes.io/blog/2020/12/02/dont-panic-kubernetes-and-docker/)
>- The image that Docker produces isn’t really a Docker-specific image, it’s an OCI (Open Container Initiative) image. Any OCI-compliant image, regardless of the tool we use to build it, will look the same to Kubernetes. Both containerd and CRI-O know how to pull those images and run them. Therefore we have a standard for what containers should look like. (https://kubernetes.io/blog/2020/12/02/dont-panic-kubernetes-and-docker/)
>
>
> Different features of Docker-compose and Kubernetes:
>
> | Docker compose                         | Kubernetes                         |
> |-----------------------------------------|-----------------------------------|
> | Manages a group of containers on a single host | Manages a cluster of hosts running any compatible container runtime |
> | Comes with Docker Desktop | Standalone project, not connected with Docker (but can be installed with Docker Desktop) |
> | Works with Docker Engine | Works with any compatible container runtime |
> | Control with `docker-compose` | Control with `kubectl` |
> | - | 	Cloud integration with providers like AWS |
> | No auto-scaling | Containers can be automatically scaled if they need more resources |
> | Product from Docker Inc. | Many distributions available, like vanilla Kubernetes, OpenShift, VMware Tanzu, k3s, microk8s |
> | docker-compose.yml | e.g.: simple-pod.yaml |
>
>
> For more information’s about the commands we can check:
>
>- https://kubernetes.io/docs/reference/kubectl/cheatsheet/
>

- Describe how the alternative tool could be used to solve the same goals as presented for this assignment (see CA4 Part-2);

> We can enable Kubernetes on docker desktop app, to do so we need to go to:
>
>- Settings -> Kubernetes -> Enable Kubernetes
>
>- To manage containers with docker we use the commands `docker` and `docker-compose`.
>- On the other hand, Kubernetes uses a tool called `kubectl` (this is installed when we activate Kubernetes on docker desktop)
>- Kubernetes commands: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands
>
> For example
>
>- To check the state of Docker Desktop cluster run the command: ` kubectl get nodes`
>- We should see a single node in the output called `docker-desktop`. That’s a full Kubernetes cluster, with a single node that runs the Kubernetes API and our own applications.
>- The Kubernetes components are running in Docker containers, but Docker Desktop doesn’t show them by default. For example, when we’re running docker commands like: `docker container ls` we’ll see zero containers.
>- If we use the command `docker info` we’ll see the containers in the running state, which are the various parts of Kubernetes.
>
> In Kubernetes YAML file we have objects, separated by ---:
>
>- A `Deployment`, describing a scalable group of identical pods.
>- A `NodePort` service, which will route traffic from port 30001 on your host to port 3000 inside the pods it routes to, allowing you to reach your bulletin board from the network.
>
> Also, notice that while Kubernetes YAML can appear long and complicated at first, but it almost always follows the same pattern:
>
>- The `apiVersion`, which indicates the Kubernetes API that parses this object
>- The `kind` indicating what sort of object this is
>- Some `metadata` applying things like names to objects
>- The `spec` specifying all the parameters and configurations of the object.
>
> 
> **Kubernetes website provides valuable help for Docker users.**
> 
> They provide a tool to convert docker-compose.yml onto files that we can use with `kubectl`
>
>- (https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/)
>- Install Kompose: `curl -L https://github.com/kubernetes/kompose/releases/download/v1.26.0/kompose-windows-amd64.exe -o kompose.exe`
>- `chmod +x kompose`
>- `sudo mv ./kompose /usr/local/bin/kompose`
>- Go to the directory containing the `docker-compose.yml`
>- To convert the `docker-compose.yml` file to files that can be used with `kubectl`, run the command `kompose convert` and then the command `kubectl apply -f <output file>`
>- Let's look up what IP the service is using: `kubectl describe svc <image_name>`
>- If you're using a cloud provider, your IP will be listed next to `LoadBalancer Ingress`: `curl http://<ip>`
> 
> 
> 
> **Bellow there's an example, the Docker website, of a yaml file created from scratch (https://docs.docker.com/get-started/kube-deploy/)**

```java
apiVersion: apps/v1
        kind: Deployment
        metadata:
        name: bb-demo
        namespace: default
spec:
        replicas: 1
        selector:
        matchLabels:
        bb: web
        template:
        metadata:
        labels:
        bb: web
        spec:
        containers:
        - name: bb-site
        image: getting-started
        ---
        apiVersion: v1
        kind: Service
        metadata:
        name: bb-entrypoint
        namespace: default
spec:
        type: NodePort
        selector:
        bb: web
        ports:
        - port: 3000
        targetPort: 3000
        nodePort: 30001
```

>  * In this case, we’ll get just one replica, or copy of the pod, and that pod (which is described under the template: key) has just one container in it.
>
> At the end of setting up the yaml file:
> 
>- Deploy the application to Kubernetes: `kubectl apply -f simple-pod.yaml`
>- List deployments: `kubectl get deployments`
>- Check services: `kubectl get services`
>
> For more informations we can use the following website: https://docs.docker.com/get-started/kube-deploy/
> 

> **BIBLIOGRAPHY**
>
>- https://kubernetes.io/docs/tutorials/kubernetes-basics/
>- https://kubernetes.io/docs/reference/kubectl/docker-cli-to-kubectl/
>- https://www.tutorialworks.com/kubernetes-vs-docker/
>- https://www.dynatrace.com/news/blog/kubernetes-vs-docker/
>- https://www.tutorialworks.com/kubernetes-vs-docker/
>- https://azure.microsoft.com/en-us/topic/kubernetes-vs-docker/
>- https://azure.microsoft.com/en-us/topic/what-is-kubernetes/#features
>

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA4/
>- git add Alternative
>- git commit -m "updated README.md CA4 Alternative Option 1 (resolves #27)"
>- git push
>- git tag ca4-alternative
>- git push origin ca4-alternative
>


**Option 2 - Explore how to deploy the solution to Heroku. See https://www.heroku.com.**

- This is not an alternative but a way to deploy your containers into the cloud 
- Present how this option can be used to deploy your solution
- Implement the deployment of your solution to Heroku


