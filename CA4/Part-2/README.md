## CA4: Part 2: Goals/Requirements

> Before start make a pull to check if repository is up to date
>
>- git pull
>
> Create CA4 folder and Part-2 folder
>
>- mkdir CA4/Part-2
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-2
>- echo “CA4: Second Week” >> README.md
>
> To make changes on the file use the command:
>
>- nano README.md
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784
>- git add CA4
>- git commit -m "created new directory CA4 and Part-2 (addresses #26)"
>- git push
>

**The goal of this assignment is to use Docker to setup a containerized environment to execute your version of the gradle version of the spring basic tutorial application**

**1. You should produce a solution similar to the one of the part 2 of the previous CA but now using Docker instead of Vagrant.**

> Copy the application folder (demo) form CA3/Part-2 to CA4/Part-2
>
>- `cp D:/SWitCH/devops-21-22-lmn-1211784/CA3/Part-2/demo D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-2`
>
>

**2. You should use docker-compose to produce 2 services/containers:**

* web: this container is used to run Tomcat and the spring application
* db: this container is used to execute the H2 server database

> Create the web and bd folder and the respective Dockerfile
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-2
>- mkdir web
>- mkdir db
>
> Create the following Dockerfiles on each folder
>
>- web Dockerfile
```java
FROM tomcat:9.0-jdk11-temurin

RUN apt-get update -y 

RUN apt-get install -y git

RUN apt-get install nodejs npm -f -y

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /tmp/build

WORKDIR /tmp/build/

RUN git clone https://MartaTrindade@bitbucket.org/martatrindade/devops-21-22-lmn-1211784.git

WORKDIR /tmp/build/devops-21-22-lmn-1211784/CA4/Part-2/demo

# Give permission to execute the application
RUN chmod u+x gradlew

RUN ./gradlew clean build && cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps && rm -Rf /tmp/build/

EXPOSE 8080
```
>- db Dockerfile
```java
FROM ubuntu:18.04

RUN apt-get update

RUN apt-get install -y openjdk-11-jdk-headless
RUN apt-get install unzip -y
RUN apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar

EXPOSE 8082
EXPOSE 9092

CMD java -cp ./h2-1.4.200.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
```
>- Create the docker-compose.yml (or .yaml)
```java
version: '3'
services:
  web:
    build: web
    ports:
      - "8080:8080"
    networks:
      default:
        ipv4_address: 192.168.56.10
    depends_on:
      - "db"
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data-backup
    networks:
      default:
        ipv4_address: 192.168.56.11
networks:
  default:
    ipam:
      driver: default
      config:
        - subnet: 192.168.56.0/24
```

> Create a backup folder for the database on the host machine:
>
>- mkdir data
>
> 
> **NOTE**: To check the IP go to:
>- Windows -> View network connections -> select netword and double click -> Details -> IPv4 Address
>- Don’t forget to update the demo/src/resources/application.properties IP on line 5
>- On the app folder update the .gitignore and remove the line that have /build
>- If the .jar file is not pushed use the following command to force: `git add -f CA4/Part-2/demo/gradle/wrapper/gradle-wrapper.jar`
>

> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-2
>- git add Part-2
>- git commit -m "copy the app folder, created web and db Dockerfile, created docker-compose.yml for CA4 Part-2 (addresses #26)"
>- git push
>

> Build the images
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-2
>- `docker-compose up`
>
> To check if everything is working see:
> 
>- In the host you can open the spring web application using the following url:
>- `http://localhost:8080/demo-0.0.1-SNAPSHOT/`
>- To open the H2 console use the following url:
>- `http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console`
>- For the connection string use: `jdbc:h2:tcp://192.168.56.11:9092/./jpadb`
>- Now we can add some data into the table, for example, type `insert into employee (id, first_name, last_name, job_years) values ( 2, 'Marta', 'Trindade', 2)` then click `Run`. The data should show on the page!
>

**3. Publish the images (db and web) to Docker Hub (https://hub.docker.com)**

> Publish the images
>
>- `docker login` (use powershell)
>- `docker tag part-2_db 1211784/part-2_db:1.0`
>- `docker push 1211784/part-2_db:1.0`
>- `docker tag part-2_db 1211784/part-2_web:1.0`
>- `docker push 1211784/part-2_web:1.0`
>

**4. Use a volume with the db container to get a copy of the database file by using the exec to run a shell in the container and copying the database file to the volume.**

>- List the containers: `docker ps` or  `docker container ls` -> 8efd3ea84a93
>- Execute a command to enter service/container: `docker-compose exec db bash` (use powershell)
>- `cp jpadb.mv.db /usr/src/data-backup`
>- To leave the container type: `exit`
>

**5. Describe the process in the readme file for this assignment. Include all the Docker files in your repository (i.e., docker-compose.yml, Dockerfile).**

> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-2
>- git add Part-2
>- git commit -m "updated README.md for CA4 Part-2 (addresses #26)"
>- git push
>

**6. At the end of this assignment mark your repository with the tag ca4-part2.**

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA4/
>- git add Part-2
>- git commit -m "updated README.md CA4 and Part-2 (resolves #26)"
>- git push
>- git tag ca4-part2
>- git push origin ca4-part2
>


