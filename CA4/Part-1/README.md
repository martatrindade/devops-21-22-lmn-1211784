## CA4: Part 1: Goals/Requirements

> Before start make a pull to check if repository is up to date
>
>- git pull
>
> Create CA4 folder and Part-1 folder
>
>- mkdir CA4
>- mkdir CA4/Part-1
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1
>- echo “CA4: First Week” >> README.md
>
> To make changes on the file use the command:
>
>- nano README.md
>
> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784
>- git add CA4
>- git commit -m "created new directory CA4 and Part-1 (addresses #25)"
>- git push
>

**The goal of the Part 1 of this assignment is to practice with Docker, creating docker images and running containers using the chat application from CA2 (see https://bitbucket.org/luisnogueira/gradle_basic_demo/)**

**The goal is to be able to package and execute the chat server in a container (developed in CA2, Part1)**

> Install Docker:
>
>- Docker Desktop - For running Docker in MacOS and Windows machines. See https://www.docker.com/products/docker-desktop. This requires Hyper-v on Windows
>- Docker Toolbox - For some Windows systems that do not support Hyper-v (i.e., Home editions) or have Hyper-v disabled. Docker Toolbox will run Docker server inside a Virtual Box VM. See https://docs.docker.com/toolbox/overview/
>- Docker Engine for Linux - Install Docker on a machine with Linux or on a VM with Linux. See https://hub.docker.com/search?q=&type=edition&offering=community&operating_system=linux
>
> Manual installation steps for older versions of WSL
>
>- https://docs.microsoft.com/en-gb/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package
>- Enable the Windows Subsystem for Linux: `dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`
>- select `Windows logo key + R`, type `winver`
>- Enable VM feature: `dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`
>- Download the Linux kernel update package:
> https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi
>- Set WSL 2 as your default version: `wsl --set-default-version 2`
>- Install your Linux distribution of choice: https://apps.microsoft.com/store/detail/ubuntu-1804-on-windows/9N9TNGVNDL3Q?hl=pt-pt&gl=PT
>- Ubuntu 18.04 for windows: `marta` `DevOps2022`
>
>- Register on Docker Hub: https://hub.docker.com/signup
>

**Attention:**

To explore the concept of docker images you should create two versions of your solution:

### Version 1

**In this version you should build the chat server "inside" the Dockerfile**

>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1
>- mkdir CA4/Part-1/Version-1
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/Version-1
>- Create Dockerfile: ` echo “#Dockerfile” >> Dockerfile`
```java
# Download base image ubuntu 18.04
FROM ubuntu:18.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Install nginx, php-fpm and supervisord from ubuntu repository
RUN apt-get update -y && apt-get install -y git \
    openjdk-11-jdk-headless \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Set working directory
WORKDIR /tmp-build

# Buil the application
RUN git clone https://MartaTrindade@bitbucket.org/luisnogueira/gradle_basic_demo.git

# Change working directory
WORKDIR /tmp-build/gradle_basic_demo

# Give permission to execute the application
RUN chmod u+x gradlew

# Build the project inside the container
RUN ./gradlew clean build
RUN cp build/libs/basic_demo-0.1.0.jar .
RUN ./gradlew clean

# Expose Port for the Application
EXPOSE 59001

# Run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

> Commit the changes and push
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1
>- git add Version-1
>- git commit -m "created new directory and Dockerfile for CA4 and Part-1 Version 1 (addresses #25)"
>- git push
>


**You should create a docker image (i.e., by using a Dockerfile) to execute the chatserver**

>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/Version-1
>- `docker build -t img_ca4_version1 .`
>
>  * If there is an error check if the build and gradle/wrapper/gradle-wrapper.jar
>  * To add go to ` cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/gradle_basic_demo` and use the command `git add -f .`
>
>- List local docker images: `docker images`
>- Start a docker container: `docker run --name [container] -p [port] -d [image]` -> `docker run --name version1 -p 59001:59001 -d img_ca4_version1`
>
>- List running containers: ` docker ps -a`
>- Run a command: `docker exec -it [container ID] [command]`
>
> Other docker commands:
>
>- Stop a running container: `docker stop [container ID]`
>- Remove a container: `docker rm [container ID]`
>- Remove an image: `docker rmi [image]`
>

**You should tag the image and publish it in docker hub**

>- `docker login`
>- Create tag: docker tag [image] [username]/[image]:[tag] -> `docker tag img_ca4_version1 1211784/img_ca4_version1`
>- Change the tag: docker tag [image] [username]/[image]:[tag] -> `docker tag img_ca4_version1 1211784/img_ca4_version1:1.0`
>- `docker push [image]:[tag]` -> docker push [username]/[image]:[tag] -> `docker push 1211784/img_ca4_version1:1.0`
>

**You should be able to execute the chat client in your host computer and connect to the chat server that is running in the container**

>- ` cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/gradle_basic_demo`
>- `./gradlew runClient`
>- The chatbox should open after running this command
> 
> At the end stop the docker: `docker stop`

### Version 2


**In this version you should build the chat server in your host computer and copy the jar file "into" the Dockerfile**

>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1
>- mkdir CA4/Part-1/Version-2
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/Version-2
>- Create Dockerfile: ` echo “#Dockerfile” >> Dockerfile`
```java
# Download base image ubuntu 18.04
FROM ubuntu:18.04

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Install java
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

# Copy the jar file into the container
COPY gradle_basic_demo/build/libs/basic_demo-0.1.0.jar .

# Expose Port for the Application
EXPOSE 59001

# Run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

> Copy the application to the Version-2 folder and Build the application (https://bitbucket.org/luisnogueira/gradle_basic_demo/)
>
>- Delete the .git folder when copying the aplication into the repository!
>- **NOTE**: The application folder should be in the same folder as the Dockerfile, otherwise the COPY command will not work.
>- `cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/Version-2/gradle_basic_demo`
>- `./gradlew clean build`
>

**You should create a docker image (i.e., by using a Dockerfile) to execute the chatserver**

> **NOTE**: Make sure the container from Version-1 is not running!
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/Version-2
>- `docker build -t img_ca4_version2 .`
>
>- List local docker images: `docker images`
>- Start a docker container: `docker run --name version2 -p 59001:59001 -d img_ca4_version2`
>

**You should tag the image and publish it in docker hub**

>- `docker login`
>- Create tag: `docker tag img_ca4_version2 1211784/img_ca4_version2:1.0`
>- `docker push 1211784/img_ca4_version2:1.0`
>

**You should be able to execute the chat client in your host computer and connect to the chat server that is running in the container**

>- ` cd D:/SWitCH/devops-21-22-lmn-1211784/CA4/Part-1/gradle_basic_demo`
>- `./gradlew runClient`
>- The chatbox should open after running this command
>

**At the end of this assignment mark your repository with the tag ca4-part1.**

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA4/
>- git add Part-1
>- git commit -m "updated README.md CA4 and Part-1 (resolves #25)"
>- git push
>- git tag ca4-part1
>- git push origin ca4-part1
>
