## CA3: Part 1: Goals/Requirements

> Before start make a pull to check if repository is up to date
>
>- git pull
>
> Create CA3 folder and Part-1 folder
>
>- mkdir CA3
>- mkdir CA3/Part-1
>
> Create README.md file
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA3/Part-1
>- echo “CA3: First Week” >> README.md
>
>To make changes on the file use the command:
>
>- nano README.md
>
> Commit the changes and push
>
>- git add CA3
>- git commit -m "created new directory CA3 and Part-1 (addresses #21)"
>- git push
>

**1. You should start by creating your VM as described in the lecture**

> To install the virtual machine, go to the following website and download the software according to the requirements: https://www.virtualbox.org/wiki/Downloads
>
> Note: Follow theoretical class PDF from page 12-19
>
> **STEP 1**
>
>- Select the button `New` -> `Name`, `Type: Linux`, `Version: Ubunto 64-bit`
>- Memory size: select at least 2048 MB
>- `Create a virtual hard disk now`
>- `VDI (Virtual Box Disk Image)`
>- `Dynamically allocated`
>- `Create`
>
> To initialize the VM click `Start`
>
>- On the pop-up window select `Cancel`
>
> **STEP 2**
>
> Now we need to change the VM settings and install the operating system:
>
>- Download the Linux Ubunto 18.04 ISO setup: https://help.ubuntu.com/community/Installation/MinimalCD
>
> On the Oracle VM select button `Settings` -> `Storage` -> `Controller: IDS/ Empty`
>
> - `Optical Drive` -> `+` -> Select the file downloaded with Linux Ubunto `mini.iso`
>- Activate checkbox `Live CD/DVD`
>- `Ok`
>
> **STEP 3**
>
>- To initialize the VM click `Start`
>- Click `Install` (attention select the option with the keyboard, not the mouse)
>- Select the options as required by Linux during installation
>- Hostname: `ubunto`
>- Choose a ubunto mirror archive close to your network, in this case we will select `Portugal`
>- On the HTTP proxy leave it blank and press `Continue`
>- After installing select the username full name: `MARTA TRINDADE` and press `Continue`
>- After installing select the username: `marta` and press `Continue`
>- After installing select a password: `DevOps2022` and press `Continue`
>- Partitioning method: `Guided - use entire disk`
>- Select the disk and press `Ok` -> `Yes`
>- PAM configuration: `No automatic updates`
>- Software selection press `Continue`
>- Install the GRUB press `Yes`
>
> Go to `Devices` -> `Optical drives` -> `Remove disk from virtual drive`
> Restart the VM (shutdown and start)
> Ubunto login
>
>- username: `marta`
>- password: ` DevOps2022`
>

>- On the Oracle VM select button `Settings` -> `Network` -> Check the NAT
>- Then go to File (top left corner) -> Host Network Manager (CTRL+H) check the host (19.168.56.1/24)
>
> Go back to Oracle VM select button `Settings` -> `Network` -> `Adapter 2`
>
>- Activate the checkbox Enable Network Adapter
>- Attached to: `Host-only Adapter`
>- Select the host and press `Ok`
>
> Now there are 2 network adapters, the Adapter 1 with NAT, and the Adapter 2 with the Host-only Ethernet Adapter, we will be using the Adapter 2

> Now lets install all the required setups to run our project (install SSH, FTP, HTTP, etc…)
>
>- Initialize the VM by pressing `Start`
>- Update the packages repositories: `sudo apt update`
>- Install the network tools: `sudo apt install net-tools`
>- Edit the network configuration file to setup the IP: `sudo nano /etc/netplan/01-netcfg.yaml`
```java
network:
    version: 2
        renderer: networkd
          ethernets:
              enp0s3:
                 dhcp4: yes
             enp0s8:
                 addresses:
                     - 192.168.56.5/24
```
>- Press `CTRL+X` -> `y` -> click enter
>- Apply the new changes: `sudo netplan apply`
>
> _Installing SSH to allow host access:_
>
>- Install openssh-server: `sudo apt install openssh-server`
>- Enable password authentication for ssh: `sudo nano /etc/ssh/sshd_config`
>	* uncomment the line `PasswordAuthentication yes`
>	* `sudo service ssh restart`
>
> _Installing FTP to allow file transfers:_
>
>- Install FTP: `sudo apt install vsftpd`
>- Enable write access for vsftpd: `sudo nano /etc/vsftpd.conf`
>	* uncomment the line `write_enable=YES`
>	* `sudo service vsftpd restart`
>
> **Note:** To access the VM through the Host terminal type: `ssh marta@192.168.56.5`
>  
> Since FTP is enabled, we can use an ftp application to transfer files, for example:
>
>- https://filezilla-project.org/
>
> _Install GIT and JDK:_
>
>- `sudo apt install git`
>- `sudo apt install openjdk-8-jdk-headless`
>- `sudo apt install openjdk-11-jdk-headless`
>

**2. You should clone your individual repository inside the VM**

> Open the VM terminal
>
> Clone the repository
>
>- git clone https://MartaTrindade@bitbucket.org/martatrindade/devops-21-22-lmn-1211784.git
>
> If you can’t access your repository go to bitbucket -> Personal Settings (top right corner after cliking the photo) -> App passwords -> Create app password (**Note** write the password because you can’t see it later)
>

**3. You should try to build and execute the spring boot tutorial basic project and the gradle_basic_demo project (from the previous assignments)**

* Attention: Do not forget to install the dependencies of the projects (e.g., git, jdk, maven, gradle, etc.)
* Attention: Also, some goals of gradle_basic_demo may not execute in the VM because it does not have a Desktop (remember that we are using the ubuntu server)!
* You should report and explain possible issues you may encounter in your readme file!

> **Note:** To access the VM through the Host terminal type: `ssh marta@192.168.56.5`
>
> **Note:** To paste commands on windows terminal use the right mouse button

> **MAVEN PROJECT FROM CA1/Part-2**
>
> Run the application (inside the alternative/demo directory)
>
>- cd ./devops-21-22-lmn-1211784/CA1/tut-react-and-spring-data-rest/basic
>- Add permission to run maven: `chmod u+x mvnw`
>- `./mvnw install`
>- `./mvnw spring-boot:run`
>- To check if it’s working go to the browser: `http://192.168.56.5:8080/`
>- To stop press: `CTRL+C`
>

> **GRADLE PROJECT FROM CA2/Part-1**
>
>- `cd ./devops-21-22-lmn-1211784/CA2/Part-1/gradle_basic_demo/`
>- Add permission to run graddle: `chmod u+x gradlew`
>- Since we created a task to delete the .jar file, we need to add it to the project on the folder gradle/wrapper (we can use FileZila, Host: 192.168.56.5, Username: marta, Password: DevOps2022)
>- Build the graddle application: `./gradlew build`
>- `nano build.gradle` -> Change from “localhost” to “192.168.56.5” -> Save the changes (CTRL+O, CTRL+X)
>- On the VM terminal type `./gradlew executeServer`
>- Host machine: ` cd /d/SWitCH/devops-21-22-lmn-1211784/CA2/Part-1/gradle_basic_demo`
>- On the host machine terminal type: `./gradlew runClient`
>- Note: we need to change the file buil.gradle on the VM and on the host machine.
>

> **GRADLE PROJECT FROM CA2/Part-2**
>
>- `cd ./devops-21-22-lmn-1211784/CA2/Part-2/demo`
>- Add permission to run graddle: `chmod u+x gradlew`
>- `./gradlew build`
>- `./gradlew bootRun`
>- To check if it’s working go to the browser: `http://192.168.56.5:8080/`
>


**4. For web projects you should access the web applications from the browser in your host machine (i.e., the "real" machine)**

> When trying to run `./mvnw install` we are faced with a error message, since we do not have permission to run that command, so first we need to add that permission by using the command `chmod u+x mvnw`.
>
> Since the VM does not have a graphic interface, we need to use the host browser and type the address ({virtualmachineIPaddress}:8080/):
>
>- In this case it will be `http://192.168.56.5:8080/`, as mentioned before on _**Exercise 2: MAVEN PROJECT FROM CA1/Part-2**_.
>

**5. For projects such as the simple chat application you should execute the server inside the VM and the clients in your host machine. _Why is this required?_**

> As mentioned before, since the virtual machine does not have a graphic interface the chatbox app will not show as it does in windows, so we need to run the client it on our host, while executing the server on the VM.
>
> Like what happened before with maven, we run into an error because we don’t have permission to run gradle, so first we will add that permission by using the command `chmod u+x gradlew`.
>
> After that, still on the VM terminal, we will type the command `./gradlew executeServer`, and on the host machine terminal type: `./gradlew runClient`.
> The chatbox will pop-up on the host machine.
>

**6. Describe the process in the readme file for this assignment. Attention: Do not commit the VM to your repository! For this part of the assignment it is only required to commit the technical documentation in the readme file.**

> Commit the changes and push
>
>- git add CA3
>- git commit -m " updated README.md CA3 and Part-1 (addresses #_)"
>- git push
>

**7. At the end of the part 1 of this assignment mark your repository with the tag ca3-part1.**

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/
>- git add Part-1
>- git commit -m "updated README.md CA3 and Part-1 (resolves #21)"
>- git push
>- git tag ca3-part1
>- git push origin ca3-part1
>
