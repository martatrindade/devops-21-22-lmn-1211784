# CA3: Part 2 Alternative with VMWare

> VMWare is a Type 2 hypervisor software
>
> Download VMWare
>
>- https://customerconnect.vmware.com/en/downloads/details?downloadGroup=WKST-PLAYER-1623-NEW&productId=1039&rPId=85399
>
>Install VMWare Utility
>
>- https://www.vagrantup.com/docs/providers/vmware/installation
>
>
> `cd D:\SWitCH\devops-21-22-lmn-1211784\ca3\AltVMWare`
>
>Install the Vagrant VMware provider plugin using the standard plugin installation procedure:
>
>- `vagrant plugin install vagrant-vmware-desktop`
>- `vagrant init vmware`
>
```java
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
end
```
>
> Windows -> Network Connections -> VMWare Network Adapter VMnet1 -> double click -> Details -> IPv4 Address
>
>- 192.168.181.10 -> web
>- 192.168.181.11 -> db
```java
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
...
  config.vm.define "db" do |db|
    db.vm.box = "hashicorp/bionic64"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.181.11"
...
  config.vm.define "web" do |web|
    web.vm.box = "hashicorp/bionic64"
    web.vm.hostname = "web"
    web.vm.network "private_network", ip: "192.168.181.10"

    # We set more ram memory for this VM
    web.vm.provider "vmware_desktop" do |v|
      v.memory = 1024
    end
...
      git clone https://MartaTrindade@bitbucket.org/martatrindade/devops-21-22-lmn-1211784.git
      cd devops-21-22-lmn-1211784/CA3/AltVMWare/demo
...
  end
end
```
> Update src/main/resources/application.properties IP address to the VM db IP (line 6)
>
>-`vagrant up --provider vmware_desktop`
>
>
> To check if it’s working to go:
>
>- http://192.168.181.10:8080/demo-0.0.1-SNAPSHOT/
>
> The H2 console can be accessed from:
>
>- http://192.168.181.10:8080/demo-0.0.1-SNAPSHOT/h2-console (web VM IP address)
>- JDBC URL: `jdbc:h2:tcp://192.168.181.11:9092/./jpadb`
>- `insert into employee (id, first_name, last_name, job_years) values ( 2, 'Marta', 'Trindade', 2)`
>
![img.png](img.png)
>Note: The repeated data is due to the hardcoded data in the file src/main/java/.../payroll/DatabaseLoader!
> 
> So it will add a new Frodo repeated line everytime we use the command: `vagrant up`