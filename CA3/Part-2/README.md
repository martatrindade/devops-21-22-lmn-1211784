## CA3: Part 2: Goals/Requirements

> Before start make a pull to check if repository is up to date
> 
>- git pull
>
> Create CA3 folder and Part-1 folder
> 
>- mkdir CA3/Part-2
>
>Create README.md file
> 
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA3/Part-2
>- echo “CA3: Second Week” >> README.md
>
>To make changes on the file use the command:
> 
>- nano README.md
>
> Commit the changes and push
> 
>- git add CA3
>- git commit -m "created new directory CA3 Part-2 (addresses #22)"
>- git push
>

**The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment to execute the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2)**

> Download and install vagrant: https://www.vagrantup.com/downloads
> 
>- To check if everything is ok, open the terminal and type `vagrant -v` (it should show the version)
> Vagrant runs on the local/host machine, not on the VM.
>- Move to the location where the folder will be created: `cd d:/SWitCH/DEVOPS `
>- Create folder where you want to initialize a vagrant project: `mkdir vagrant-project-1`
>- `cd d:/SWitCH/DEVOPS/vagrant-project-1`
>- Create a new Vagrant configuration file for the project: `vagrant init envimation/ubuntu-xenial` (the name of the box will be what it’s written after the word init)
>- To start the virtualization type `vagrant up`
>- To see the status type `vagrant status`
>- To enter the VM using a SSH session type `vagrant ssh` (VM manager will show the box running)
>- To exit the VM type `exit`
>- To stop the box type `vagrant halt` (**Note**: this command cannot be used inside the VM, first we need to use the `exit` command.
>- To see the files on vagrant folder first check the `pwd`, the vagrant folder is at the same level as the home folder.
>- To test how vagrant works with new files use `vagrant ssh` and inside the vagrant folder try creating a test file `touch test.txt`. Then type `exit` and check on the host machine if the file was also created
>
> Now let’s use Vagrant to setup a VM with a webserver.
> 
>- Clone the repository: https://github.com/atb/vagrant-basic-example
>- `cd D:/SWitCH/DEVOPS`
>- `git clone https://github.com/atb/vagrant-basic-example.git`
>- `cd D:/SWitCH/DEVOPS/vagrant-basic-example`
>- `vagrant up`
>- Go to http://localhost:8010/ (it should show an example page)
>- To play around and test type `code .` (**Note**: this only works if VisualStudioCode is installed)
>- When all the changes are made type `vagrant halt`
>- The command `vagrant up --provision` should be used every time there are changes to the Vagrantfile to make sure the changes are applied.
>
>- For more information check Vagrant Quick Reference: https://gist.github.com/wpscholar/a49594e2e2b918f4d0c4
>- To save space on the host machine, destroy the VM by typing: `vagrant destroy –f` (**Note**: this will remove all the VM files except the Vagrantfile. To create the VM again type `vagrant up`
>- To remove the box from the local file system type `vagrant box remove envimation/ubuntu-xenial`
>

**1. You should use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution**

> Clone the repository from: https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/
>
>- `cd D:/SWitCH/DEVOPS`
>- `git clone https://MartaTrindade@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git`
>- Delete the .git folder and copy into the repository
>- `cd D:/SWitCH/DEVOPS/vagrant-multi-spring-tut-demo`
>- `rm -rf .git/`
>
> Copy the Vagrantfile to CA3 Part-2 folder
> 
>- `cp -R /d/SWitCH/DEVOPS/vagrant-multi-spring-tut-demo /d/SWitCH/devops-21-22-lmn-1211784/CA3/Part-2/`
>
> Commit the changes and push
> 
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/
>- git add Part-2
>- git commit -m "updated CA3 and Part-2 (addresses #22)"
>- git push
>

**2. Study the Vagrantfile and see how it is used to create and provision 2 VMs:**
* web: this VM is used to run tomcat and the spring boot basic application
* db: this VM is used to execute the H2 server database

>- `cd d:/SWitCH/DEVOPS/vagrant-multi-spring-tut-demo`
>- To open and study the Vagrantfile type `code .`
>
> Looking at the Vagrantfile we can see that:
>- From line 17-41 we have the specific configurations for the database VM, the **db**
>- From line 44-78 we have the specific configurations for the webserver VM, the **web**
>

**3. Copy this Vagrantfile to your repository (inside the folder for this assignment)**

> Copy the folder to CA2/Part-2 to CA3/Part-2 folder
> 
>- `cp -R /d/SWitCH/ devops-21-22-lmn-1211784/CA2/Part-2/demo /d/SWitCH/devops-21-22-lmn-1211784/CA3/Part-2/`
>

**4. Update the Vagrantfile configuration so that it uses your own gradle version of the spring application.**

Issue when cloning a private repository in the provision section of the Vagrantfile:

When we execute a git clone of a private repository, git will ask for authentication using a prompt for reading the password

* This prompt for reading the password will break the provision script, since it should execute without any user interaction
* The simplest way to avoid this issue is to make the repository public, such as displayed in the next figure
* Therefore, you should make your repository public before running your Vagrantfile (in Bitbucket go to Repository Settings/Repository Details)

> On Bitbucket go to `Repository Settings` -> `Repository details` -> deactivate `Access level`
>
>- Go to Vagrantfile and change
> 
> * Line 12: sudo apt-get install openjdk-11-jdk-headless -y
> * Line 70: git clone https://MartaTrindade@bitbucket.org/martatrindade/devops-21-22-lmn-1211784.git
> * Line 71: cd devops-21-22-lmn-1211784/CA3/Part-2/demo
> * Line 75: sudo cp ./build/libs/demo-0.0.1-SNAPSHOT.jar /var/lib/tomcat8/webapps
>
>- `cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/Part-2`
>- `vagrant up`
>
> An error regarding JDK11 stops the operation.
>
> Since the previous VM (ubuntu-xenial) does not support JDK11, because of the ubuntu version, we have to solutions:
>
>1. Change the application to run on JDK8
>2. Use ubuntu/bionic64
>
> In this case we will use the second approach, since the objective is to work with vagrant and not the application itself.
>

> Commit the changes and push
> 
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/
>- git add Part-2
>- git commit -m "updated CA3 and Part-2 (addresses #22)"
>- git push
>

**5. Check https://bitbucket.org/atb/tut-basic-gradle to see the changes necessary so that the spring application uses the H2 server in the db VM. Replicate the changes in your own version of the spring application.**

> Files to change:
> 
>- Vagrantfile (detailed bellow)
>- demo/build.gradle (plugins line 6 added id war; dependencies line 22 to 26 added lines; frontend updated)
```java
plugins {
	id 'org.springframework.boot' version '2.6.6'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
	id "org.siouan.frontend-jdk11" version "6.0.0"
	id 'war'
}
…
dependencies {
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	implementation 'org.springframework.boot:spring-boot-starter-data-rest'
	implementation 'org.springframework.boot:spring-boot-starter-thymeleaf'
	runtimeOnly 'com.h2database:h2'
	testImplementation ('org.springframework.boot:spring-boot-starter-test') {
		exclude group: 'org.junit.vintage', module: 'junit-vintage-engine'
	}
	// To support war file for deploying to tomcat
	providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'
}
…
frontend {
    nodeVersion = "14.17.3"
    assembleScript = "run webpack"
}
```
>-demo/package.json (updates scripts)
```java
"scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
  }
```
>- demo/src/main/java/com/greglturnquist/payroll/ServletInitializer.java (create file)
```java
package com.greglturnquist.payroll;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ReactAndSpringDataRestApplication.class);
    }

}
```
>- demo/src/main/resources/application.properties
```java
server.servlet.context-path=/demo-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
#spring.datasource.url=jdbc:h2:mem:jpadb
# In the following settings the h2 file is created in /home/vagrant folder
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
# So that spring will no drop de database on every execution.
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true
```
>- src/main/js/app.js (line 17-18 update path)
```java
componentDidMount() { // <2>
		client({method: 'GET', path: '/demo-0.0.1-SNAPSHOT/api/employees'}).done(response => {
			this.setState({employees: response.entity._embedded.employees});
		});
	}
```
>- src/main/resources/templates/index.html (line 6 removed / before main.css)
```java
<head lang="en">
    <meta charset="UTF-8"/>
    <title>ReactJS + Spring Data REST</title>
    <link rel="stylesheet" href="main.css" />
</head>
```
>
>- Go to Vagrantfile and change
>   * Line 4, 19, 46 to: `config.vm.box = "ubuntu/bionic64"`; `db.vm.box = "ubuntu/bionic64"`; `web.vm.box = "ubuntu/bionic64"`
>
>- Delete the .jar files so when the command `./gradlew clean built` runs, it will create the new .war files required.
>- `cd d/SWitCH/devops-21-22-lmn-1211784/CA3/Part-2/demo`
>- `./gradlew clean build`
>
> Run vagrant and see if everything is working, the frontend should show the table made in the previous CA2 Part2 class
>
>- `cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/Part-2`
>- `vagrant destroy` (This step is required otherwise ubuntu/bionic64 will not install and the vagrant will use ubunto-xenial)
>- `vagrant up`
>
> To check if it’s working to go:
>
>- http://localhost:8080/demo-0.0.1-SNAPSHOT/
>- http://192.168.56.10:8080/demo-0.0.1-SNAPSHOT/
>
> The H2 console can be accessed from:
>
>- http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
>- http://192.168.56.10:8080/demo-0.0.1-SNAPSHOT/h2-console
>- **Note**: On JDBC URL type: `jdbc:h2:tcp://192.168.56.11:9092/./jpadb`
>- Now we can add some data into the table, for example, type `insert into employee (id, first_name, last_name, job_years) values ( 2, 'Marta', 'Trindade', 2)` then click `Run`. The data should show on the page!
>
>![img.png](img.png)

> **NOTE: How to fix HTTP Status 404**
> 
![img_1.png](img_1.png)
>
> When running vagrant inside the path /var/lib/tomcat8/webapps there should be the demo-0.0.1-SNAPSHOT. To check this use `vagrant ssh web`
>
> If the file is not there, we can copy them manually, but the most probable cause is the repository was not made public. Make sure to check the repository is public (see exercice 4)!
>
> DO NOT FORGET TO COMMIT BEFORE RUNNING `vagrant up`
> 
> The hardcoded data on the `DatabaseLoader.java` will be added to the db everytime we run the command `vagrant up`
> 

**6. Describe the process in the readme file for this assignment.**

> Commit the changes and push
> 
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/
>- git add Part-2
>- git commit -m "updated CA3 and Part-2 (addresses #22)"
>- git push
>

**7. At the end of the part 2 of this assignment mark your repository with the tag ca3-part2.**

> Commit the changes and push
> 
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/
>- git add Part-2
>- git commit -m "updated README.md CA3 and Part-2 (resolves #22)"
>- git push
>- git tag ca3-part2
>- git push origin ca3-part2
>
