## CA3: Part 2: Alternative Solution

> Change current working directory
>
>- cd D:/SWitCH/devops-21-22-lmn-1211784/
>
> Create CA3 folder and Part-1 folder
> 
>- mkdir CA3/Alternative
>
> Create README.md file
> 
>- cd D:/SWitCH/devops-21-22-lmn-1211784/CA3/Alternative
>- echo “CA3: Alternative” >> README.md
>
> To make changes on the file use the command:
> 
>- nano README.md
>
> Commit the changes and push
> 
>- git add CA3
>- git commit -m "created new directory CA3 Alternative (addresses #23)"
>- git push
>

**For this assignment you should present an alternative technological solution for the virtualization tool (i.e., an hypervisor alternative to VirtualBox).**

> The virtualization software, also known as virtual machine (VM) software, is a type of software that allows us to create and run virtual machines on physical servers. VM are also known as hypervisor. A hypervisor allows one host computer to support multiple guest VMs by virtually sharing its resources, such as memory and processing.
>
> There are 2 types of hypervisors:
>
>- Type 1: acts like a lightweight operating system and runs directly on the host’s hardware
>- Type 2: runs as a software layer on an operating system, like another program
>
> There are several virtualization tools in the market, for this assignment we will focus on the ones that are recommended on vagrant website.
>
> Vagrant have 4 main virtualization tool providers: VirtualBox, VMware, Hyper-V and Docker, but it also works with custom providers.
>
> Note: Vagrant default provider is VirtualBox, so in order to use another provider, first we need to install it, and then change the Vagrantfile configurations.
>
> After installation we can find configurations support on vagrant website, for example: https://app.vagrantup.com/boxes/search?utf8=%E2%9C%93&sort=downloads&provider=virtualbox&q=hyperV
>

**1. Present how the alternative tool compares to VirtualBox regarding virtualization features;**

> In this case we will use Hyper-V as an alternative too.
>
> Both VirtualBox and Hyper-V are free.
>
> **ABOUT VIRTUALBOX**:
>
>- Oracle VM VirtualBox is a type 2 hypervisor, which runs on the host operating system
>- It can run on several operating systems, including Windows, Linux, and MacOS.
>- In addition to running on multiple host operating systems, Oracle VM VirutalBox can also create VMs using multiple guest operating systems, rather than just Windows.
>
> **ABOUT HYPER-V**:
>
>- Hyper-V is a type 1 hypervisor that manages operating systems by running directly on a computer’s hardware.
>- It can only be installed on Windows-based systems.
>- It offers high performance virtual machines and can output a lot of power depending on the hardware that it is running on.
>- Since it’s a type 1 hypervisor, VMs are always running if the hardware turned on.
>- It also integrates well with Windows infrastructures and, once implemented, it’s simple to use once.
>
![img.png](img.png)

**2. Describe how the alternative tool could be used with vagrant to solve the same goals as presented for this assignment (see previous slides);**

>- Setup Hyper-V
>
>  * Hyper-V must be enabled prior to using the provider (run powershell as admin): ` Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All`
>
>- **Warning**: Enabling Hyper-V will cause VirtualBox, VMware, and any other virtualization technology to no longer work. See how to create a boot entry to boot Windows without Hyper-V enabled, if there will be times you will need other hypervisors: https://www.hanselman.com/blog/switch-easily-between-virtualbox-and-hyperv-with-a-bcdedit-boot-entry-in-windows-81
>- Copy demo application from CA3/Part-2 to CA3/Alternative folder
>
>  * `cp -R /d/SWitCH/DEVOPS/CA3/Part-2 /d/SWitCH/devops-21-22-lmn-1211784/CA3/Alternative/`
>
>- Copy and update Vagrantfile to run with Hyper-V
```java
Vagrant.configure("2") do |config|
      config.vm.box = "hashicorp/bionic64"
      config.vm.provider "hyperv"
      config.vm.network "public_network"
      config.vm.synced_folder ".", "/vagrant", disabled: true
      config.vm.provider "hyperv" do |h|
        h.enable_virtualization_extensions = true
        h.linked_clone = true
      end
```
>- Run `vagrant up –provider=hyperv`
>

**To potentially achieve a complete solution of the assignment you should also implement the alternative design presented in the previous item 2.**

>- Open powershell as admin and run: `Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All`
>- Go to BIOS and enable hardware assisted virtualization (note: computer must be shutdown and powered off after changing the bios settings. Rebooting only won’t work)
> * In BIOS -> Advanced -> CPU Configuration -> Virtualization Technology -> Enable
>- Go to windows features and activate all Hyper-V options and restart the computer
>- Copy demo application from CA3/Part-2 to CA3/Alternative folder
>
>  * `cp -R /d/SWitCH/DEVOPS/CA3/Part-2/demo /d/SWitCH/devops-21-22-lmn-1211784/CA3/Alternative/`
>
>- `cd d/SWitCH/devops-21-22-lmn-1211784/CA3/Alternative`
>- Update Vagrantfile
```java
Vagrant.configure("2") do |config|
      config.vm.box = "hashicorp/bionic64"
      config.vm.provider "hyperv"
      config.vm.network "public_network"
      config.vm.synced_folder ".", "/vagrant", disabled: true
      config.vm.provider "hyperv" do |h|
        h.enable_virtualization_extensions = false
        h.linked_clone = true
      end
…
  #============
  # Configurations specific to the database VM
  config.vm.define "db" do |db|
    db.vm.box = "hashicorp/bionic64"
    db.vm.hostname = "db"
…
  end

  #============
  # Configurations specific to the webserver VM
  config.vm.define "web" do |web|
    web.vm.box = "hashicorp/bionic64"
    web.vm.hostname = "web"

    # We set more ram memory for this VM
    web.vm.provider "hyperv" do |v|
      v.memory = 1024
    end
…
  cd devops-21-22-lmn-1211784/CA3/Alternative/demo
…
end
```
>- Due to vagrant limitations, it cannot enforce a static IP automatically, since it does not know how to create and configure new networks for Hyper-V.
> So when running `vagrant up` it will prompt ask what virtual switch we want to connect the VM to.
>
>- Open powershell as an administrator
>- `vagrant up`
>- Note: we can create each VM independently by typing `vagrant up db` or `vagrant up web`
>- Select the virtual switch (default)
>- To access the VM type `vagrant ssh` and specify the vm you want to access
>- `vagrant halt` 
>- Check on Hyper-V Manager the IP address for the db and web VM and update the links or use the command
   > `Get-VM | Select-Object -ExpandProperty NetworkAdapters | Select-Object VMName,IPAddresses`
>
> VMName                             | IPAddresses
> ------                             | -----------
> Alternative_db_1651514166289_66614 | 172.24.23.24, fe80::215:5dff:fe01:80c
> Alternative_web_1651514387928_87032| 172.24.21.178, fe80::215:5dff:fe01:80d
> 
>  * On demo/src/main/resources/application.properties update the IP for the VM db IP (line 5)
```java
spring.datasource.url=jdbc:h2:tcp://172.17.2.148:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
```
>  * On the browser link replace the previous ID for the VM web IP
>
>- After updating the files use the command `vagrant up --provision`
> 
> To check if it’s working to go:
>
>- http://`172.24.21.178`:8080/demo-0.0.1-SNAPSHOT/
>
> The H2 console can be accessed from:
>
>- http://`172.24.21.178`:8080/demo-0.0.1-SNAPSHOT/h2-console (web VM IP address)
>- **Note**: On JDBC URL type: `jdbc:h2:tcp://172.24.23.24:9092/./jpadb` (db VM IP address)
>
>- Like we did on CA3/Part-2 we can add some data into the table, for example, type `insert into employee (id, first_name, last_name, job_years) values ( 2, 'Marta', 'Trindade', 2)` then click `Run`. The data should show on the page!
>

> **TROUBLESHOOTING**
> 
>- If the page isn’t loading the problem may be the IP address, to check the VMs IP address use the command: `Get-VM | Select-Object -ExpandProperty NetworkAdapters | Select-Object VMName,IPAddresses`
>- After checking the IP update the file demo/src/main/resources/application.properties and insert the VM db IP address (line 5)
>- If we need to restart tomcat use the commands `sudo service tomcat8 stop` and then `sudo service tomcat8 start`
>- Tip: For more information how to set Hyper-v VM we check the following youtube link: https://www.youtube.com/watch?v=sr9pUpSAexE&ab_channel=TheDigitalLife
>
 
> **CONCLUSION**
>
> Although Hyper-V is already installed in windows there’s quite a bit of configurations we need to do before start using it, including changes in BIOS.
>
> Comparing to VirtualBox it’s harder to setup, this is also due to the different types of hypervisors.
>
> Regarding the Vagrantfile setup there are not many changes we need to apply to make it work. We basically just needed to change the `|config|` to specify the provider to run with Hyper-V.
> In contrast while running with VirtualBox we did not need to specify it as a provider since it’s the default one.
> 
> A great disadvantage is the impossibility of setting a static IP, meaning everytime we restart our host machine or destroy and rebuilt the VMs we need to update the IP in src/main/resources/application.properties
> Se we need to do `vagrant up` the first time, check our VM generated IP, do `vagrant halt`, update the files and then `vagrant up --provision`.
> 

> Commit the changes and push
>
>- cd /d/SWitCH/devops-21-22-lmn-1211784/CA3/
>- git add Alternative
>- git commit -m "updated README.md CA3 Alternative (resolves #23)"
>- git push
>- git tag ca3-alternative
>- git push origin ca3-alternative
>


